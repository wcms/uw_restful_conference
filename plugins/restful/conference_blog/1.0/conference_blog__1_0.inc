<?php

/**
 * @file
 * Contains Conference Blog plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Blog'),
  'resource' => 'conference_blog',
  'name' => 'conference_blog__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_blog',
  'description' => t('Export the conference blog content type.'),
  'class' => 'RestfulConferenceBlogResource',
);
