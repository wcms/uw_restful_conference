<?php

/**
 * @file
 * Contains main class for Conference Blog plugin for UW Restful Conference.
 */

/**
 * Export the uw blog content type.
 */
class RestfulConferenceBlogResource extends RestfulEntityBaseConferenceNode {

  /**
   * Overrides \RestfulEntityBase::defaultSortInfo().
   */
  public function defaultSortInfo() {
    // Sort by 'order' field in descending order.
    return array('published' => 'DESC');
  }

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );

    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );

    $public_fields['title'] = array(
      'property' => 'title',
    );

    $public_fields['summary'] = array(
      'property' => 'body',
      'sub_property' => 'summary',
    );

    $public_fields['body'] = array(
      'property' => 'body',
      'sub_property' => 'value',
      'process_callbacks' => array(
        array($this, 'contentSource'),
      ),
    );

    $public_fields['featured_image'] = array(
      'property' => 'field_event_image',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
    );

    $public_fields['related_posts'] = array(
      'property' => 'field_conference_related_posts',
      'sub_propery' => 'nid',
      'process_callbacks' => array(
        array($this, 'getEntityNodes'),
      ),
    );

    $public_fields['categories'] = array(
      'property' => 'field_conference_blog_category',
      'sub_property' => 'tid',
      'process_callbacks' => array(
        array($this, 'getEntityTerm'),
      ),
    );

    $public_fields['metatags'] = array(
      'property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );

    $public_fields['author'] = array(
      'property' => 'field_author',
      'process_callbacks' => array(
        array($this, 'getAuthor'),
      ),
    );

    $public_fields['published'] = array(
      'property' => 'field_blog_date',
    );

    $public_fields['sticky'] = array(
      'property' => 'sticky',
    );

    return $public_fields;
  }

  /**
   * Get entity's author information.
   *
   * @param array
   *   The entity author.
   *
   * @return object
   *   The author information.
   */
  protected function getAuthor($author_values) {
    $profile_prefix = 'people-profiles/';
    $node_prefix    = 'node/';
    $author_url     = (!empty($author_values['url'])) ? $author_values['url'] : '';
    $author_title   = (!empty($author_values['title'])) ? $author_values['title'] : NULL;
    $author         = new stdClass();
    $values         = NULL;
    $node           = NULL;

    if (substr($author_url, 0, strlen($profile_prefix)) == $profile_prefix) {
      $path = drupal_lookup_path("source", $author_url);
      $node = menu_get_object("node", 1, $path);
    }
    elseif (substr($author_url, 0, strlen($node_prefix)) == $node_prefix) {
      $node = menu_get_object("node", 1, $author_url);
    }

    if ($node) {
      $values = entity_metadata_wrapper('node', $node);
    }

    $name = ($values) ? $values->title->value() : $author_title;
    $segs = explode(' ', $name);
    $first_name = $segs[0];
    $last_name = (count($segs) > 1) ? $segs[1] : NULL;

    $author->id = ($values) ? $values->nid->value() : NULL;
    $author->label = $name;
    $author->title = $name;
    $author->first_name = $first_name;
    $author->last_name = $last_name;
    $author->position = ($values) ? $values->field_profile_title->value() : NULL;
    $author->affiliation = ($values) ? $values->field_profile_affiliation->value() : NULL;
    $author->twitter_handle = ($values) ? $values->field_conference_twitter_handle->value() : NULL;
    $author->bio = ($values) ? $values->body->value->value() : NULL;
    $author->summary = ($values) ? $values->body->summary->value() : NULL;
    $author->avatar = ($values) ? $this->imageProcess($values->field_profile_photo->value()) : NULL;
    $author->url = ($values) ? NULL : $author_url;

    return $author;
  }

  /**
   * Overrides parent filter the query for list.
   *
   * @param \entityfieldquery $query
   *   The query object.
   */
  protected function queryforlistfilter(\entityfieldquery $query) {
    foreach ($this->parserequestforlistfilter() as $filter) {
      if ($filter['public_field'] == 'categories' && !is_numeric($filter['value'][0])) {
        // Category is the name. Replace it with its tid.
        // TODO: Use machine name instead.
        $request = $this->getRequest();
        $terms = taxonomy_get_term_by_name(str_replace('-', '%20', $filter['value'][0]), 'conference_blog_categories');

        if (!empty($terms)) {
          $term = array_shift(array_slice($terms, 0, 1));
          $request['filter']['categories'] = $term->tid;
          $this->setRequest($request);
        }
      }
      elseif ($filter['public_field'] == 'topics' && !is_numeric($filter['value'][0])) {
        // Topic is the name. Replace it with its tid.
        // TODO: Use machine name instead.
        $request = $this->getRequest();
        $terms = taxonomy_get_term_by_name(str_replace('-', '%20', $filter['value'][0]), 'uw_blog_tags');

        if (!empty($terms)) {
          $term = array_shift(array_slice($terms, 0, 1));
          $request['filter']['topics'] = $term->tid;
          $this->setRequest($request);
        }
      }
      elseif ($filter['public_field'] == 'title') {
        $request = $this->getRequest();
        $request['filter']['title'] = array(
          'value' => $filter['value'][0],
          'operator' => 'CONTAINS',
        );
        $this->setRequest($request);
      }
    }

    parent::queryforlistfilter($query);
  }

}
