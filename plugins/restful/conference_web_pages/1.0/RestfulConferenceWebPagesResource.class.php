<?php

/**
 * @file
 * Contains main class for Conference Web Pages plugin for UW Restful
 * Conference.
 */

/**
 * Export uw web page content type.
 */
class RestfulConferenceWebPagesResource extends RestfulEntityBaseConferenceNode {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['display_wide_screen'] = array(
      'property' => 'uw_page_settings_node',
    );
    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );

    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );

    $public_fields['home'] = array(
      'callback' => array($this, 'getHomeStatus'),
    );

    $public_fields['feature'] = array(
      'property' => 'field_conference_feature',
    );

    $public_fields['title'] = array(
      'property' => 'title',
    );

    $public_fields['body'] = array(
      'property' => 'body',
      'sub_property' => 'value',
      'process_callbacks' => array(
        array($this, 'contentSource'),
      ),
    );

    $public_fields['summary'] = array(
      'property' => 'body',
      'sub_property' => 'summary',
    );

    $public_fields['featured_image'] = array(
      'property' => 'field_event_image',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
    );

    $public_fields['metatags'] = array(
      'property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );

    return $public_fields;
  }

  /**
   * Get entity's home status.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return bool
   *   The home status
   */
  protected function getHomeStatus(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    return (variable_get('site_frontpage', 'node/1') == ('node/' . $values->nid));
  }

}
