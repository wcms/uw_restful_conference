<?php

/**
 * @file
 * Contains Conference Web Page plugin for UW Restful Plugin.
 */

$plugin = array(
  'label' => t('Conference Web Page'),
  'resource' => 'conference_web_pages',
  'name' => 'conference_web_pages__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_web_page',
  'description' => t('Export the UW web page content type.'),
  'class' => 'RestfulConferenceWebPagesResource',
);
