<?php

/**
 * @file
 * Contains Conference Blog Categories plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Blog Categories'),
  'resource' => 'conference_blog_categories',
  'name' => 'conference_blog_categories__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'conference_blog_categories',
  'description' => t('Export the blog categories.'),
  'class' => 'RestfulConferenceBlogCategoriesResource',
);
