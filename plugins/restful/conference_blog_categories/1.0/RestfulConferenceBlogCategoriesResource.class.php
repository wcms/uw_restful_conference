<?php

/**
 * @file
 * Contains main class for Conference Blog Categories plugin for UW Restful
 * Conference.
 */

/**
 * Export the conference blog categories taxonomy.
 */
class RestfulConferenceBlogCategoriesResource extends RestfulEntityBaseConferenceTaxonomyTerm {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();
    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );
    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );
    $public_fields['description'] = array(
      'property' => 'description',
      'sub_property' => 'value',
    );
    $public_fields['name'] = array(
      'property' => 'name',
    );
    $public_fields['metatags'] = array(
      'property' => 'tid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );
    return $public_fields;
  }

}
