<?php

/**
 * @file
 * Contains Conference Speaker Groups plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Speaker Groups'),
  'resource' => 'conference_speaker_groups',
  'name' => 'conference_speaker_groups__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'conference_speaker_groups',
  'description' => t('Export the conference speaker groups.'),
  'class' => 'RestfulConferenceSpeakerGroupsResource',
);
