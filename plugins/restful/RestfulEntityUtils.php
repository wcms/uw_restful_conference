<?php

/**
 * @file
 * Contains RestfulEntityBaseConferenceNode.
 */

 /**
  * This file contains some useful utilities for transforming data that can
  * be consumed in the construction of RESTful endpoints.
  */
class RestfulEntityUtils {

  /**
   * Return speaker objects with specific properties of the nodes.
   *
   * @param array $nodes
   *   An array of nodes (bundle: conference_speaker)
   *
   * @return array
   *   An array of the processed nodes
   */
  public static function processSpeakers($nodes) {
    $speakers = array();

    if (!$nodes || !is_array($nodes) || empty($nodes)) {
      return $speakers;
    }

    foreach ($nodes as $node) {
      $speakers[] = self::processSpeaker($node);
    }

    return $speakers;
  }

  /**
   * Return a speaker object with specific properties of the node.
   *
   * @param object $node
   *   The node (bundle: conference_speaker)
   *
   * @return object
   *   The processed node
   */
  public static function processSpeaker($node) {
    if (!$node || empty($node)) {
      return NULL;
    }

    $speaker = new stdClass();
    $values = entity_metadata_wrapper('node', $node);

    $speaker->id = $values->nid->value();
    $speaker->label = $values->title->value();
    $speaker->title = $values->title->value();
    $speaker->first_name = $values->field_first_name->value();
    $speaker->last_name = $values->field_last_name->value();
    $speaker->position = $values->field_profile_title->value();
    $speaker->affiliation = $values->field_profile_affiliation->value();
    $speaker->twitter_handle = $values->field_conference_twitter_handle->value();
    $speaker->bio = $values->body->value->value();
    $speaker->summary = $values->body->summary->value();
    $speaker->avatar = RestfulEntityBaseConferenceNode::processImage($values->field_profile_photo->value());

    return $speaker;
  }

  /**
   * Returns term objects with specific properties of the term.
   *
   * @param array $terms
   *   The terms.
   *
   * @return array
   *   The processed terms
   */
  public static function processTerms($terms) {
    $results = array();

    foreach ($terms as $term) {
      $result = self::processTerm($term);
      if ($result) {
        $results[] = $result;
      }
    }

    return $results;
  }

  /**
   * Returns a term object with specific properties of the term.
   *
   * @param object $term
   *   The term.
   *
   * @return object
   *   The processed term
   */
  public static function processTerm($term) {
    if (is_array($term)) {
      $term = $term[0];
    }

    $result = new stdClass();
    $values = entity_metadata_wrapper('taxonomy_term', $term->tid);

    $result->id = $values->tid->value();
    $result->label = $values->name->value();
    $result->name = $values->name->value();
    $result->description = $values->description->value();

    return $result;
  }

  /**
   * Delegates to RestfulEntityBaseConferenceNode::processImage($value)
   */
  protected function processImage($value) {
    return RestfulEntityBaseConferenceNode::processImage($value);
  }

}
