<?php

/**
 * @file
 * Contains Conference Links plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Links'),
  'resource' => 'conference_links',
  'name' => 'conference_links__1_0',
  'entity_type' => 'node',
  'bundle' => 'conference_links',
  'description' => t('Export the conference links content type.'),
  'class' => 'RestfulConferenceLinksResource',
);
