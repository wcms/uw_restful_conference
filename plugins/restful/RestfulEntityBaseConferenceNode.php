<?php

/**
 * Extends RestfulEntityBaseNode with utility methods for transforming data.
 */
class RestfulEntityBaseConferenceNode extends RestfulEntityBaseNode {

  /**
   * Get entity's source.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return string
   *   The source URL.
   */
  protected function getEntitySource(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    return $values->path['source'];
  }

  /**
   * Get entity's alias.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return string
   *   The alias URL.
   */
  protected function getEntityAlias(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();

    if (variable_get('site_frontpage', 'node/1') == ('node/' . $values->nid)) {
      return '';
    }
    else {
      return drupal_get_path_alias('node/' . $values->nid);
    }
  }

  /**
   * Get entity's term.
   *
   * @param int $tid
   *   The term id.
   *
   * @return array
   *   The term.
   */
  protected function getEntityTerm($tid) {
    $term   = new stdClass();
    $values = entity_metadata_wrapper('taxonomy_term', $tid);

    $term->id = $values->tid->value();
    $term->label = $values->name->value();
    $term->name = $values->name->value();
    $term->description = $values->description->value();
    $term->weight = $values->weight->value();

    return $term;
  }

  /**
   * Get sponsor display order.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return float
   *   The sponsor display order.
   */
  protected function getSponsorDisplayOrder(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    $nid = $values->nid;
    $node = new stdClass();
    $values = entity_metadata_wrapper('node', $nid);
    $order = $values->field_conference_sponsor_order->value();
    $level_weight = $values->field_conference_sponsor_level->value();
    // If field_conference_sponsor_level field is NOT select, we set 100 as its
    // weight. field_conference_sponsor_order field value divided by 10000.
    $level_weight = isset($level_weight->weight) ? $level_weight->weight : 100;
    return $level_weight + $order / 10000;
  }

  /**
   * Get entity's terms.
   *
   * @param array $tids
   *   The term ids.
   *
   * @return array
   *   The terms.
   */
  protected function getEntityTerms($tids) {
    $terms = array();

    foreach ($tids as $tid) {
      $term = $this->getEntityTerm($tid);
      if ($term) {
        $terms[] = $term;
      }
    }

    return $terms;
  }

  /**
   * Get entity's node.
   *
   * @param int $nid
   *   The node id.
   *
   * @return array
   *   The node.
   */
  protected function getEntityNode($nid) {
    $node   = new stdClass();
    $values = entity_metadata_wrapper('node', $nid);

    $node->id = $values->nid->value();
    $node->alias = $this->getEntityAlias($values);
    $node->source = $this->getEntitySource($values);
    $node->title = $values->title->value();
    $node->body = $values->body->value->value();
    $node->summary = $values->body->summary->value();
    $node->featured_image = $this->imageProcess($values->field_event_image->value());
    $node->published = $values->created->value();

    return $node;
  }

  /**
   * Get entity's nodes.
   *
   * @param array $nids
   *   The node ids.
   *
   * @return array
   *   The nodes.
   */
  protected function getEntityNodes($nids) {
    $nodes = array();

    foreach ($nids as $nid) {
      $node = $this->getEntityNode($nid);
      if ($node) {
        $nodes[] = $node;
      }
    }

    return $nodes;
  }

  /**
   * Get entity's metatags information.
   *
   * @param int $nid
   *   The entity id.
   *
   * @return array
   *   The metatag information.
   */
  protected function getMetaTags($nid) {
    $return_array = array();
    $wrapper = entity_metadata_wrapper('node', $nid);
    $metadata_array = metatag_generate_entity_metatags($wrapper->value(), 'node');
    foreach ($metadata_array as $metadata) {
      // Check if there is a value set for this $metadata.
      if (isset($metadata['#attached']['drupal_add_html_head'][0][0]['#value'])) {
        $name = $metadata['#attached']['drupal_add_html_head'][0][0]['#name'];
        $value = $metadata['#attached']['drupal_add_html_head'][0][0]['#value'];
        $return_array[$name] = $value;
      }
      // Reset the title to '[node:title]' from
      // '[page:current-page:title] | [site:name]'.
      // Front end added the ' | [site:name]' to the end.
      $return_array['title'] = $wrapper->title->value();
      $return_array['og:title'] = $wrapper->title->value();
      $return_array['twitter:title'] = $wrapper->title->value();
      $return_array['itemprop:name'] = $wrapper->title->value();
      if (isset($return_array['og:url'])) {
        // Set the url to the values listed below.
        $meta_url = $wrapper->url->value();
        $return_array['og:url'] = $meta_url;
        $return_array['twitter:url'] = $meta_url;
        $return_array['canonical'] = $meta_url;
      }
    }
    return $return_array;
  }

  /**
   * Process callback, Remove Drupal specific items from the image array.
   *
   * @param array $value
   *   The image array.
   *
   * @return array
   *   A cleaned image array.
   */
  protected function imageProcess($value) {
    if (isset($value)) {

      if (static::isArrayNumeric($value)) {
        $output = array();
        foreach ($value as $item) {
          $output[] = $this->imageProcess($item);
        }
        return $output;
      }
    }
    else {
      return "";
    }

    // Add caption from image_field_caption module if present.
    $caption = (isset($value['image_field_caption']) ? $value['image_field_caption']['value'] : NULL);

    $image_styles = array(
      'original',
      'thumbnail',
      'medium',
      'large',
      'size540x370',
      'cover_photo',
      'cover_photo_x2',
      'conference_advertisement',
      'conference_sponsor',
      'conference_speaker',
      'conference_masthead',
    );

    $return_value = array(
      'id' => $value['fid'],
      'self' => file_create_url($value['uri']),
      'filemime' => $value['filemime'],
      'filesize' => $value['filesize'],
      'width' => $value['width'],
      'height' => $value['height'],
      'alt' => $value['alt'],
      'title' => $value['title'],
      'styles' => array(),
      'caption' => $caption,
    );

    foreach ($image_styles as $style_name) {
      $return_value['styles'][$style_name] = image_style_url($style_name, $value['uri']);
    }

    return $return_value;
  }

  /**
   * Static alias for imageProcess.
   *
   * @param array $value
   *   The image array.
   *
   * @return array
   *   A cleaned image array.
   */
  public static function processImage($value) {
    return self::imageProcess($value);
  }

  /**
   * Overrides \RestfulEntityBase::getControllers()
   */
  public function getControllers() {
    $controllers = parent::getControllers();
    $controllers['\w+'] = array(
      \RestfulInterface::GET => 'viewEntities',
    );
    return $controllers;
  }

  /**
   * Overrides \RestfulEntityBase::viewEntity().
   *
   * Wrap viewEntity to load the node via alias. So we can use the alias
   * as the ID.
   */
  public function viewEntity($label) {
    if (is_numeric($label) || empty($label)) {
      return parent::viewEntity($label);
    }
    else {
      $path = drupal_lookup_path("source", $label);
      $node = menu_get_object('node', 1, $path);

      if (!$node) {
        throw new \RestfulBadRequestException(format_string('Label @label is not a valid entity ID.', array('@label' => $label)));
      }

      return parent::viewEntity($node->nid);
    }
  }

  /**
   * Process callback for formatting the content for formatting the content.
   *
   * @param string $value
   *   The current value.
   *
   * @return string
   *   The new value.
   */
  protected function contentSource($value) {
    $filterValue = ckeditor_socialmedia_filter_process($value);
    $filterValue = uw_video_embed_filter_process($filterValue);
    if (module_exists('uw_ct_embedded_facts_and_figures') && function_exists('cke_ff_process')) {
      $filterValue = cke_ff_process($filterValue, NULL, NULL, NULL, NULL, NULL);
    }

    return $filterValue;
  }

}
