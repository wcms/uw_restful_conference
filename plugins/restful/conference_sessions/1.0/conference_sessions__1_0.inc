<?php

/**
 * @file
 * Contains Conference Sessions plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Sessions'),
  'resource' => 'conference_sessions',
  'name' => 'conference_sessions__1_0',
  'entity_type' => 'node',
  'bundle' => 'conference_session',
  'description' => t('Export the conference session content type.'),
  'class' => 'RestfulConferenceSessionsResource',
);
