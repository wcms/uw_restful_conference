<?php

/**
 * @file
 * Contains main class for Conference Videos plugin for UW Restful Conference.
 */

/**
 * Export conference video content type.
 */
class RestfulConferenceVideosResource extends RestfulEntityBaseConferenceNode {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );

    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );

    $public_fields['title'] = array(
      'property' => 'title',
    );

    $public_fields['sticky'] = array(
      'property' => 'sticky',
    );

    $public_fields['promote'] = array(
      'property' => 'promote',
    );

    $public_fields['video_title'] = array(
      'property' => 'field_conference_video_link',
      'sub_property' => 'title',
    );

    $public_fields['video_url'] = array(
      'property' => 'field_conference_video_link',
      'sub_property' => 'url',
    );

    $public_fields['metatags'] = array(
      'property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );

    return $public_fields;
  }

}
