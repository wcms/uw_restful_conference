<?php

/**
 * @file
 * Contains Conference Web Form plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Web Form'),
  'resource' => 'conference_web_forms',
  'name' => 'conference_web_forms__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_web_form',
  'description' => t('Export the UW web form content type.'),
  'class' => 'RestfulConferenceWebFormsResource',
);
