<?php

/**
 * @file
 * Contains Conference Main Menu plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Main Menu'),
  'resource' => 'conference_main_menu',
  'name' => 'conference_main_menu__1_0',
  'entity_type' => 'menu_link',
  'bundle' => 'main-menu',
  'description' => t('Export the conference main menu.'),
  'class' => 'RestfulConferenceMainMenuResource',
);
