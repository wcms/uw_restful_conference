<?php

/**
 * @file
 * Contains main class for Conference Main Menu plugin for UW Restful
 * Conference.
 */

/**
 * Export the main menu.
 */
class RestfulConferenceMainMenuResource extends RestfulEntityBaseMenu {

  /**
   * Get the content type of the entity.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return string
   *   The content type.
   */
  protected function getContentType(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    $nid = $this->getNodeFromAlias($values->link_path);
    if ($nid) {
      $node = node_load($nid);
      return node_type_get_name($node);
    }

    return '';
  }

  /**
   * The feature of the entity.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return string
   *   The feature.
   */
  protected function getPageFeature(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    $nid = $this->getNodeFromAlias($values->link_path);
    if ($nid) {

      $node = node_load($nid);
      $type = node_type_get_name($node);

      if ($type === 'Web Page') {
        $node_values = entity_metadata_wrapper('node', $node);
        return $node_values->field_conference_feature->value();
      }
    }

    return NULL;
  }

}
