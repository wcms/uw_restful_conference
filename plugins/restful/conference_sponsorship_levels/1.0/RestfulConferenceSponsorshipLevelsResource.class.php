<?php

/**
 * @file
 * Contains main class for Conference Sponsorship Levels plugin for UW
 * Restful Conference.
 */

/**
 * Export conference sponsorship levels taxonomy with sponsor content type
 * nested in each level sort by Conference Sponsorship Levels taxonomy weight asc.
 */
class RestfulConferenceSponsorshipLevelsResource extends RestfulEntityBaseConferenceTaxonomyTerm {

  /**
   * Overrides \RestfulEntityBase::defaultSortInfo().
   */
  public function defaultSortInfo() {
    // Sort by taxonomy weight.  This is not setup if taxonomy terms
    // are in a hierarchy.
    return array('weight' => 'ASC');
  }

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['weight'] = array(
      'property' => 'weight',
    );

    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );

    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );

    $public_fields['sponsors'] = array(
      'property' => 'tid',
      'process_callbacks' => array(
        array($this, 'getSponsors'),
      ),
    );

    $public_fields['description'] = array(
      'property' => 'description',
    );

    $public_fields['name'] = array(
      'property' => 'name',
    );

    $public_fields['metatags'] = array(
      'property' => 'tid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );

    return $public_fields;
  }

  /**
   * Get entity's sponsors.
   *
   * @param integer
   *   The entity id.
   *
   * @return array
   *   The sponsors for this sponsorship level order by field_conference_sponsor_order.
   */
  protected function getSponsors($tid) {
    $return_array = array();

    $sponsors = taxonomy_select_nodes($tid, $pager = FALSE, $limit = FALSE);
    foreach ($sponsors as $sponsor) {
      $node = node_load($sponsor);
      $values = entity_metadata_wrapper('node', $node);
      $key = $values->field_conference_sponsor_order->value();
      $return_array[] = array(
        'id' => $node->nid,
        'label' => $node->title,
        'title' => $node->title,
        'order' => $values->field_conference_sponsor_order->value(),
        'logo' => $this->imageProcess($values->field_conference_sponsor_logo->value()),
        'link' => $values->field_conference_sponsor_url->value(),
      );
    }

    usort($return_array, function ($a, $b) {
      return $a['order'] - $b['order'];
    });

    return $return_array;
  }

  /**
   * Process callback, Remove Drupal specific items from the image array.
   *
   * @param array $value
   *   The image array.
   *
   * @return array
   *   A cleaned image array.
   */
  protected function imageProcess($value) {
    return RestfulEntityBaseConferenceNode::processImage($value);
  }

}
