<?php

/**
 * @file
 * Contains Conference Sponsorship Levels plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Sponsorship Levels'),
  'resource' => 'conference_sponsorship_levels',
  'name' => 'conference_sponsorship_levels__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'conference_sponsorship_levels',
  'description' => t('Export the conference sponsorship levels.'),
  'class' => 'RestfulConferenceSponsorshipLevelsResource',
);
