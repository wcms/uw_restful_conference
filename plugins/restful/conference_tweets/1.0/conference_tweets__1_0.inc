<?php

/**
 * @file
 * Contains Conference Tweets plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Tweets'),
  'resource' => 'conference_tweets',
  'name' => 'conference_tweets__1_0',
  'description' => t('Expose latest tweets about the conference.'),
  'class' => 'RestfulConferenceTweetsResource',
  'render_cache' => array(
    'render' => TRUE,
  ),
);
