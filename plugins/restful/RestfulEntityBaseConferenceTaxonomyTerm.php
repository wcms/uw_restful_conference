<?php

/**
 * @file
 * Contains RestfulEntityBaseConferenceTaxonomyTerm.
 */

/**
 * This base class extends RestfulEntityBaseTaxonomyTerm providing in addition,
 * some useful utility methods for transforming data.
 */
class RestfulEntityBaseConferenceTaxonomyTerm extends RestfulEntityBaseTaxonomyTerm {

  /**
   * Get entity's source.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return string
   *   The source URL.
   */
  protected function getEntitySource(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    return 'taxonomy/term/' . $values->tid;
  }

  /**
   * Get entity's alias.
   *
   * @param \EntityDrupalWrapper $wrapper
   *   The wrapped entity.
   *
   * @return string
   *   The alias URL.
   */
  protected function getEntityAlias(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    return drupal_lookup_path('alias', 'taxonomy/term/' . $values->tid);
  }

  /**
   * Get entity's metatags information.
   *
   * @param integer
   *   The entity id.
   *
   * @return array
   *   The metatag information.
   */
  protected function getMetaTags($tid) {
    $return_array = array();
    $wrapper = entity_metadata_wrapper('taxonomy_term', $tid);
    $metadata_array = metatag_generate_entity_metatags($wrapper->value(), 'taxonomy_term');
    foreach ($metadata_array as $metadata) {
      // Check if there is a value set for this $metadata.
      if (isset($metadata['#attached']['drupal_add_html_head'][0][0]['#value'])) {
        $name = $metadata['#attached']['drupal_add_html_head'][0][0]['#name'];
        $value = $metadata['#attached']['drupal_add_html_head'][0][0]['#value'];
        $return_array[$name] = $value;
      }
      if (isset($return_array['og:url'])) {
        // Set the url to the values listed below.
        $meta_url = $wrapper->url->value();
        $return_array['og:url'] = $meta_url;
        $return_array['twitter:url'] = $meta_url;
        $return_array['canonical'] = $meta_url;
      }
      // Reset the title to '[node:title]' from '[page:current-page:title] | [site:name]'.
      // The front end adds the [site:name] afterwords.
      $return_array['title'] = $wrapper->name->value();
      $return_array['og:title'] = $wrapper->name->value();
      $return_array['twitter:title'] = $wrapper->name->value();
      $return_array['itemprop:name'] = $wrapper->name->value();

    }
    return $return_array;
  }

}
