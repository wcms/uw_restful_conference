<?php

/**
 * @file
 * Contains Conference Session Types plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Session Types'),
  'resource' => 'conference_session_types',
  'name' => 'conference_session_types__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'conference_session_types',
  'description' => t('Export the conference session types.'),
  'class' => 'RestfulConferenceSessionTypesResource',
);
