<?php

/**
 * @file
 * Contains Conference Blog Tags plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Blog Tags'),
  'resource' => 'conference_blog_tags',
  'name' => 'conference_blog_tags__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_blog_tags',
  'description' => t('Export the blog tags.'),
  'class' => 'RestfulConferenceBlogTags',
);
