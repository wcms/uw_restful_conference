<?php

/**
 * @file
 * Contains main class for Conference Meta Data plugin for UW Restful
 * Conference.
 */

/**
 * Export meta data (settings).
 */
class RestfulConferenceMetaDataResource extends \RestfulDataProviderVariable {

  /**
   * {@inheritdoc}
   */
  public function publicFieldsInfo() {
    return array();
  }

  /**
   * Get entity's metatags information.
   *
   * @param integer
   *   The entity id.
   *
   * @return array
   *   The metatag information.
   */
  protected function getMetaTags($nid) {
    $return_array = array();
    $wrapper = entity_metadata_wrapper('node', $nid);
    $metadata_array = metatag_generate_entity_metatags($wrapper->value(), 'node');
    foreach ($metadata_array as $metadata) {
      // Check if there is a value set for this $metadata.
      if (isset($metadata['#attached']['drupal_add_html_head'][0][0]['#value'])) {
        $name = $metadata['#attached']['drupal_add_html_head'][0][0]['#name'];
        $value = $metadata['#attached']['drupal_add_html_head'][0][0]['#value'];
        $return_array[$name] = $value;
      }
      if (isset($return_array['og:url'])) {
        // Set the url to the values listed below.
        $meta_url = $wrapper->url->value();
        $return_array['og:url'] = $meta_url;
        $return_array['twitter:url'] = $meta_url;
        $return_array['canonical'] = $meta_url;
      }
      // Reset the title to '[node:title]' from '[page:current-page:title] | [site:name]'.
      // Front end added the ' | [site:name]' to the end.
      $return_array['title'] = $wrapper->title->value();
      $return_array['og:title'] = $wrapper->title->value();
      $return_array['twitter:title'] = $wrapper->title->value();
      $return_array['itemprop:name'] = $wrapper->title->value();
    }
    return $return_array;
  }

  /**
   * {@inheritdoc}
   */
  public function mapVariableToPublicFields($variable) {
    // Strip the $variable down to just publication settings.
    return $variable['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function getVariablesForList() {
    // Get all variable start with this value.
    $starting_value = 'conference_';
    // Need to change range to the total number of variables.
    $this->setRange($this->getTotalCount());
    // Use parent funtion to grab the complete list.
    $variables = parent::getVariablesForList();
    // Go through the list and unset all of the undesirable variables.
    foreach ($variables as $index => $variable) {
      if (substr($variable['name'], 0, strlen($starting_value)) !== $starting_value) {
        unset($variables[$index]);
      }
    }

    // Get all variable start with this value.
    $starting_value = 'ckeditor_socialmedia_';
    // Need to change range to the total number of variables.
    $this->setRange($this->getTotalCount());
    // Use parent funtion to grab the complete list.
    $variables2 = parent::getVariablesForList();
    // Go through the list and unset all of the undesirable variables.
    foreach ($variables2 as $index => $variable) {
      if (substr($variable['name'], 0, strlen($starting_value)) !== $starting_value) {
        unset($variables2[$index]);
      }
      else {
        $variables[] = $variables2[$index];
      }
    }

    $conference_site_logo = theme_get_setting('logo', 'uw_conference_frontend');
    $default_site_logo    = !!(strpos($conference_site_logo, 'uw_conference_frontend/logo.png') !== FALSE);

    $variables[] = array(
      'name'  => 'conference_site_logo',
      'value' => $conference_site_logo,
    );

    $variables[] = array(
      'name'  => 'conference_site_favicon',
      'value' => theme_get_setting('favicon', 'uw_conference_frontend'),
    );

    $variables[] = array(
      'name'  => 'conference_site_name',
      'value' => variable_get('site_name', 'UW Conference'),
    );

    $variables[] = array(
      'name'  => 'conference_site_slogan',
      'value' => variable_get('site_slogan', ''),
    );

    $variables[] = array(
      'name'  => 'conference_site_loaded',
      'value' => TRUE,
    );

    $variables[] = array(
      'name'  => 'conference_site_default_logo',
      'value' => $default_site_logo,
    );

    return $variables;
  }

  /**
   * {@inheritdoc}
   */
  public function index() {
    // Get all settings/variables that start with conference_.
    $prefix = 'conference_';

    // Define sections that settings will be pushed into.
    $sections = array(
      'agenda',
      'analytics',
      'contact_information',
      'links',
      'location',
      'mailchimp_block',
      'mailchimp',
      'social',
      'content_block',
      'blog',
      'theme',
      'tint',
      'twitter_feed',
      'videos',
      'speakers',
      'ckeditor_socialmedia',
    );

    $variables = $this->getVariablesForList();

    foreach ($variables as $variable) {
      // Set empty values to NULL.
      if (empty($variable['value']) || (is_array($variable['value']) && empty($variable['value']['value']))) {
        $variable['value'] = NULL;
      }

      $name = str_replace($prefix, '', $variable['name']);

      // Special exception for logo.
      if ($name === 'logo') {
        if ($file = file_load($variable['value'])) {
          $url = file_create_url($file->uri);
          $settings[$name] = $url;
        }
        else {
          $settings[$name] = NULL;
        }
      }
      elseif ($name === 'location_marker_icon') {
        if ($file = file_load($variable['value'])) {
          $url = file_create_url($file->uri);
          $settings['location']['marker_icon'] = $url;
        }
        else {
          $settings['location']['marker_icon'] = NULL;
        }
      }
      elseif ($name === 'favicon') {
        if ($file = file_load($variable['value'])) {
          $url = file_create_url($file->uri);
          $settings[$name] = $url;
        }
        else {
          $settings[$name] = NULL;
        }
      }
      elseif ($name === 'banner') {
        if ($file = file_load($variable['value'])) {
          $url = file_create_url($file->uri);
          $settings[$name] = $url;
        }
        else {
          $settings[$name] = NULL;
        }
      }
      else {
        $found = FALSE;

        // Add this setting to a section if applicable.
        foreach ($sections as $section) {
          $section_prefix = $section . '_';

          if (substr($name, 0, strlen($section_prefix)) === $section_prefix) {
            $found = TRUE;
            $name = str_replace($section_prefix, '', $name);
            $settings[$section][$name] = is_array($variable['value']) ? $variable['value']['value'] : $variable['value'];
            break;
          }
        }

        // Otherwise add it to the top level of the settings object.
        if (!$found) {
          $settings[$name] = is_array($variable['value']) ? $variable['value']['value'] : $variable['value'];
        }
      }
    }

    $return_array['settings'] = $settings;

    $nid = isset($_GET['nid']) ? $_GET['nid'] : NULL;

    if (!$nid) {
      $frontpage = variable_get('site_frontpage', 'node/1');
      $normal = drupal_get_normal_path($frontpage);
      $parts = explode('/', $normal);
      $nid = $parts[1];
    }

    if ($nid) {
      $return_array['metatags'] = $this->getMetaTags($nid);
    }

    // Filter out some private things...
    unset($return_array['twitter_consumer_key']);
    unset($return_array['twitter_consumer_secret']);
    unset($return_array['twitter_oauth_access_token']);
    unset($return_array['twitter_oauth_access_token_secret']);
    unset($return_array['twitter_query_string']);

    return $return_array;
  }

}
