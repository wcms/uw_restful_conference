<?php

/**
 * @file
 * Contains Conference Meta Data plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Meta Data'),
  'resource' => 'conference_meta_data',
  'name' => 'conference_meta_data__1_0',
  'description' => t('Expose conference meta data to the REST API.'),
  'class' => 'RestfulConferenceMetaDataResource',
  'render_cache' => array(
    'render' => TRUE,
  ),
);
