Purpose
=======

This module builds custom endpoints for the conference website.

More about this module
-----------------------

- Depends on https://github.com/RESTful-Drupal/restful 1.x
- Custom class for making menu end points, which depends on https://www.drupal.org/project/entity_menu_links to turn menus into entities
  - There is quirks about this dependency module, as it does not turn 'system' menu items into entities (which include one added through views ui).

Other Notes
-----------

When creating a new endpoint (or pulling it down on a production server), you need to clear cache twice. This is a problem with Drupal and CTools caching all known classes before caching new plugins.
